import pandas as pd
import numpy as np
from geopy.distance import vincenty
from geopy.units import degrees, nautical
from utils.sql import GET_GPS_USER
from utils.utils import fetch_data, dict_to_sql, all_groups
from collections import defaultdict
import sys
from sklearn.cluster import DBSCAN
from sqlalchemy.types import *

#HDBSCAN doesn't like me anymore
# import hdbscan


def update_location(t1, t2, conn, device_id, device_type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    location_data = fetch_data(device_id,GET_GPS_USER,conn,t1,t2)


    #If there is no record for this user:
    if location_data is None:
        return

    #Cluster and label results
    clustered = cluster_and_label(location_data, eps = distance_to_degrees(10), min_samples = 10 )
    #Do we want to do this?
    clustered = clustered.resample("15min").ffill()	
    results = all_groups(clustered,LOCATION_FUNCTIONS, device_id=device_id)
    dict_to_sql(results, "location", LOCATION_SQL_TYPES, conn)


def cluster_and_label(df, weigh_dups = True , **kwargs):
    """

    :param df:   a df with columns "latitude", "longitude", and "datetime"
                                     or
               a df with comlumns "latitude","longitude" and a datetime index
    :param weigh_dups: If true remove duplicate points and weight them during
                        clustering. This can dramatically reduce memory usage.
    :param kwargs: arguments for sklearn's DBSCAN
    :return: a new df of labeled locations with moving points removed, where the cluster
             labeled as "1" is the largest, "2" the second largest, and so on
    """
    location_data = df
    if not isinstance(df.index, pd.DatetimeIndex):
        location_data = df.set_index("datetime")

    stationary = remove_moving(location_data,1)
    clusterer = DBSCAN(**kwargs)

    if weigh_dups:
        counts_df = stationary[["latitude" ,"longitude"]].groupby(["latitude" ,"longitude"]).size().reset_index()
        counts = counts_df[0]
        lat_lon = counts_df[["latitude","longitude"]].values
        cluster_results = clusterer.fit_predict(lat_lon, sample_weight= counts)

        #Need to extend labels back to original df without weights
        counts_df["location_label"] = cluster_results
        # remove the old count column
        del counts_df[0]

        merged = pd.merge(stationary,counts_df, on = ["latitude" ,"longitude"])

        #Now compute the label mapping:
        cluster_results = merged["location_label"].values
        no_noise = cluster_results[np.where(cluster_results != -1)]
        label_map = rank_count_map(no_noise)

        #And remap the labels:
        merged.index = stationary.index
        stationary["location_label"] = merged["location_label"].map(label_map)


    else:

        lat_lon = stationary[["latitude","longitude"]].values
        cluster_results = clusterer.fit_predict(lat_lon)

        #We don't want to count noise
        no_noise = cluster_results[np.where(cluster_results != -1)]
        label_map = rank_count_map(no_noise)

        stationary["location_label"] = pd.Series(cluster_results, index = stationary.index).map(label_map)

    return stationary

def rank_count_map(l, return_dict = False):
    """ Returns a function which will map each element of a list 'l' to its rank,
    such that the most common element maps to 1

    Is used in this context to sort the cluster labels so that cluster 1 is the most
    visited.

    If return_dict, return a mapping dict rather than a function

    If a function, if the value can't be found label as -1

    """
    labels, counts = tuple(np.unique(l, return_counts = True))
    sorted_by_count = [x for (y,x) in sorted(zip(counts, labels), reverse = True)]
    label_to_rank = {label : rank + 1 for (label, rank) in [(sorted_by_count[i],i) for i in range(len(sorted_by_count))]}

    if return_dict:
        return  label_to_rank

    else:
        return lambda x: label_to_rank.get(x, -1)

def vincenty_row(x):
    """
    :param x: A row from a dataframe
    :return: The distance in meters between
    """

    try:
       return vincenty((x['_lat_before'], x['_lon_before']),
                 (x['_lat_after'], x['_lon_after'])).meters

    except UnboundLocalError:
        return 0


def remove_moving(df, v):

    if not df.index.is_monotonic:
        df = df.sort_index()

    lat_lon_temp = pd.DataFrame()

    lat_lon_temp['_lat_before'] = df.latitude.shift()
    lat_lon_temp['_lat_after'] =  df.latitude.shift(-1)
    lat_lon_temp['_lon_before'] = df.longitude.shift()
    lat_lon_temp['_lon_after'] =  df.longitude.shift(-1)

    #
    distance = lat_lon_temp.apply( vincenty_row, axis = 1) / 1000
    time = (df.reset_index().datetime_EST.shift(-1) - df.reset_index().datetime_EST.shift()).fillna(-1) / np.timedelta64(1,'s') / (60.*60)
    time.index = distance.index.copy()
    return df[(distance / time) < v]

def distance_to_degrees(d):
    #Just an approximation, but speeds up clustering by a huge amount and doesnt introduce much error
    #over small distances
    return degrees(arcminutes=nautical(meters= d))


def most_frequent_loc(g):

    if g.empty:
        return None
    result = g["location_label"].mode()
    if result.empty:
        return g["location_label"].iloc[0]
    else:
        return result[0]


def time_in_global_most_frequent_place_minutes(g):
    return int(g[g["location_label"] == 1]["location_label"].count()) * 15

def time_in_global_second_place_minutes(g):
    return int(g[g["location_label"] == 2]["location_label"].count()) * 15


def number_unique_locations(g):
    return g["location_label"][g["location_label"] != -1].nunique()

def travel_distance_meters(g):

    lat_lon_temp = pd.DataFrame()

    lat_lon_temp['_lat_before'] = g.latitude
    lat_lon_temp['_lat_after'] =  g.latitude.shift(-1)
    lat_lon_temp['_lon_before'] = g.longitude
    lat_lon_temp['_lon_after'] =  g.longitude.shift(-1)
    lat_lon_temp["location_label"] = g["location_label"]

    not_noise = (lat_lon_temp["location_label"] != -1)
    change = (lat_lon_temp["location_label"].shift(-1).fillna(0) != lat_lon_temp["location_label"])
    changes_selector = (not_noise & change)
    # print len(changes_selector)
    # print len(lat_lon_temp)
    return lat_lon_temp.apply(vincenty_row, axis = 1)[changes_selector].sum()

def radius_of_gyration(g):
    #Center is the centroid, nor the home location
    not_noise = g[g["location_label"] != -1]
    changes_selector = (not_noise["location_label"].shift() != not_noise["location_label"])
    mobility_trace = not_noise[changes_selector]

    #Average x,y
    lat_lon = mobility_trace[["latitude","longitude"]].values
    center = np.average(lat_lon, axis = 0)
    norm = np.linalg.norm(lat_lon - center)
    return np.sqrt(norm) / len(lat_lon)

def location_entropy(g):
    #Get percentages for each location
    percents = g["location_label"].value_counts(normalize= True)
    return -1 * percents.map(lambda x: x * np.log(x)).sum()




LOCATION_FUNCTIONS = [
    most_frequent_loc,
    time_in_global_most_frequent_place_minutes,
    time_in_global_second_place_minutes,
    number_unique_locations,
    travel_distance_meters,
    radius_of_gyration,
    location_entropy 
]

LOCATION_SQL_TYPES = {
    "most_frequent_loc" : Integer,
    "time_in_global_most_frequent_place_minutes": Integer,
    "time_in_global_second_place_minutes" : Integer,
    "number_unique_locations" : Integer,
    "travel_distance_meters" : Integer,
    "radius_of_gyration" : Float,
    "location_entropy" : Float
}
