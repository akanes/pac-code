import pandas as pd
import numpy as np
from utils.sql import *
from utils.utils import *
import urllib2
from bs4 import BeautifulSoup
import time
from sqlalchemy.types import *

def update_app(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection objec    :param device_id: A device_id for which the analysis will be run:q!

    :param type: one of either IOS or ANDROID
    :return: None
    """

    #I'm guessing that we dont care about system apps
    app_data = fetch_data(device_id,GET_APPS_WITH_CATEGORIES_USER,conn,t1,t2)
    # app_data = fix_times(app_data,cols = ["double_end_timestamp"])
    if app_data is None:
        return
    results = all_groups(app_data,APP_FUNCTIONS, device_id= device_id)
    dict_to_sql(results, "app", APP_SQL_TYPES, conn)

def most_common_app(g):
    if g.empty:
        return None
    result = g["package_name"].mode()
    if result.empty:
        return g["package_name"].iloc[0]
    else:
        return result[0]

def most_common_category(g):
    if g.empty:
        return None
    result = g["package_category"].mode()
    if result.empty:
        return g["package_category"].iloc[0]
    else:
        return result[0]


def number_of_unique_apps(g):
    return g["package_name"].nunique()

def apps_per_minute(g):
    count = g["package_name"].count()
    minutes = (g.index.max() - g.index.min()).total_seconds() / 60.

    if minutes == 0:
        return 0
    else:
        return count / minutes

def app_use_time_seconds(g):
    active = g[g["process_importance"] == 100]
    duration = (active["double_end_timestamp"] - active["timestamp"]) / 1000
    total_duration = duration.sum()
    return int(total_duration)

def number_of_app_changes(g):
    #Is this right? need to consult Aware Docs
    #Ask Afsaneh about this - do we only want to look at the active ones? For all of these?
    return g[g["process_importance"] == 100]["package_name"].count()


APP_FUNCTIONS = [
    number_of_unique_apps,
    apps_per_minute,
    app_use_time_seconds,
    number_of_app_changes,
    most_common_category,
    most_common_app
]
APP_SQL_TYPES = {
    "most_common_category" : String(50),
    "number_of_unique_apps" : Integer,
    "apps_per_minute" : Float,
    "app_use_time_seconds" : Integer,
    "number_of_app_changes" : Integer,
    "most_common_app" : String(50)
}

def query_google(appid):

    """
    Querys the google play store to find the package category given a package name.
    If the package can't be found on the store, return the string "None"
    :param appid: A package name
    :return: The google play package category
    """

    time.sleep(1)

    url = "https://play.google.com/store/apps/details?id=" + appid
    print "Finding {0}".format(appid)

    try:
        response = urllib2.urlopen(url)
    except urllib2.HTTPError:
        return "None"

    soup = BeautifulSoup(response, 'html.parser')
    text = soup.find('span', itemprop='genre')



    if text:
        return text.contents[0]
    else:
        return "NoneOrUtility"



def update_app_categories(conn):
    """Maintains a SQL table of app ids and their category. Due to Google rate limits,
    only one request can be made a second

    """

    #NEED TO AUTO CREATE

    to_update = pd.read_sql(GET_UN_UPDATED_IDS,conn)

    print "Adding {} package categories...".format(len(to_update["package_name"]))
    to_update["package_category"] = to_update["package_name"].map(query_google)

    print "Saving to database..."
    to_update.to_sql(CATEGORY_TABLE,conn, index = False, if_exists="append")
    print "Done"




