from utils.utils import *
from utils.sql import GET_ACCELEROMETER_USER
import numpy as np
def update_accelerometer(t1, t2, conn, device_id, type):

    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    accel_data = fetch_data(device_id,GET_ACCELEROMETER_USER,conn,t1,t2)


    #If there is no record for this user:
    if accel_data is None:
        return

    #Add magnitude column
    accel_values = accel_data[["double_values_0","double_values_1","double_values_2"]].values
    accel_data["magnitude"] = np.linalg.norm(accel_values, axis =1)
    results = all_groups(accel_data,ACCELEROMETER_FUNCTIONS, device_id=device_id)
    dict_to_sql(results, "accelerometer",ACCELEROMETER_SQL_TYPES, conn)


def mean_magnitude(g):
    return np.mean(g.magnitude)

def std_magnitude(g):
    return np.std(g.magnitude)

def median_magnitude(g):
    return np.median(g.magnitude)

def min_magnitude(g):
    return np.min(g.magnitude)

def max_magnitude(g):
    return  np.max(g.magnitude)

ACCELEROMETER_FUNCTIONS = [
    mean_magnitude,
    std_magnitude,
    median_magnitude,
    min_magnitude,
    max_magnitude,
]

ACCELEROMETER_SQL_TYPES ={
    "mean_magnitude" : Float,
    "std_magnitude"  : Float,
    "median_magnitude" : Float,
    "min_magnitude" : Float,
    "max_magnitude" : Float
}