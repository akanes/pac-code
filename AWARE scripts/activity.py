
from utils.sql import  GET_ACTIVITY_USER
from utils.utils import  *
from sqlalchemy.types import *
import pandas as pd


def update_activity(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    activity_data = fetch_data(device_id,GET_ACTIVITY_USER,conn,t1,t2)
    if activity_data is None:
        return
    #Need to add a column to indicate change in activity from previous frame
    activity_data["changed"] = (activity_data.activity_type != activity_data.activity_type.shift())
    results = all_groups(activity_data, ACTIVITY_FUNCTIONS, device_id=device_id)
    dict_to_sql(results,"activity",ACTIVITY_SQL_TYPES,conn)


def count_changes(g):
    """Applied to each groupby object to count the changes"""
    return (g["changed"][g["changed"] == True]).count()

def number_of_activities(g):
    return g["activity_type"].nunique()

def most_common_activity(g):
    if g.empty:
        return None
    result = g["activity_name"].mode()
    if result.empty:
        return g["activity_name"].iloc[0]
    else:
        return result[0]




ACTIVITY_FUNCTIONS = [
    count_changes,
    number_of_activities,
    most_common_activity
]

ACTIVITY_SQL_TYPES = {
    "count_changes" : Integer,
    "number_of_activities" : Integer,
    "most_common_activity" : String(length=20)

}
