import pandas as pd
import numpy as np
from utils.sql import GET_MESSAGES_USER, GET_DEVICES, MESSAGE_TABLE
from utils.utils import *
from sqlalchemy.types import Integer

def update_message(t1, t2, conn, device_id, device_type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    message_data = fetch_data(device_id,GET_MESSAGES_USER,conn,t1,t2)
    if message_data is None:
        return  pd.DataFrame()
    results = all_groups(message_data, MESSAGE_APPLY, device_id = device_id)
    dict_to_sql(results, "message", MESSAGE_SQL_TYPES, conn)

def number_of_outgoing_messages(g):
    return g["message_type"][g["message_type"] == 2].count()

def number_of_incoming_messages(g):
    return g["message_type"][g["message_type"] == 1].count()

def most_frequent_correspondent(g):
    if g.empty:
        return None
    result = g["trace"].mode()
    if result.empty:
        return g["trace"].iloc[0]
    else:
        return result[0]

def number_of_correspondents(g):
    return g["trace"].nunique()

MESSAGE_APPLY = [
    number_of_outgoing_messages,
    number_of_incoming_messages,
    most_frequent_correspondent,
    number_of_correspondents
]

MESSAGE_SQL_TYPES = {
    "number_of_outgoing_messages" : Integer,
    "number_of_incoming_messages" : Integer,
    "most_frequent_correspondent" : Text,
    "number_of_correspondents"    : Integer
}