from utils.utils import *
from utils.sql import GET_ROTATION_USER
import numpy as np
def update_temperature(t1, t2, conn, device_id, type):

    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    temp_data = fetch_data(device_id,GET_ROTATION_USER,conn,t1,t2)


    #If there is no record for this user:
    if temp_data is None:
        return

    #Add magnitude column
    results = all_groups(temp_data, TEMPERATURE_FUNCTIONS, device_id=device_id)
    dict_to_sql(results, "temperature",TEMPERATURE_SQL_TYPES, conn)


def mean_temperature(g):
    return np.mean(g.temperature)

def std_temperature(g):
    return np.std(g.temperature)

def median_temperature(g):
    return np.median(g.temperature)

def min_temperature(g):
    return np.min(g.temperature)

def max_temperature(g):
    return  np.max(g.temperature)

TEMPERATURE_FUNCTIONS = [
    mean_temperature,
    std_temperature,
    median_temperature,
    min_temperature,
    max_temperature,
]

TEMPERATURE_SQL_TYPES ={
    "mean_temperature" : Float,
    "std_temperature"  : Float,
    "median_temperature" : Float,
    "min_temperature" : Float,
    "max_temperature" : Float
}