from sqlalchemy import schema, types
from utils.sql import  *

#Sets up the necessary tables to store features


def setup_tables(engine):
    """
    Sets up the necessary tables to store features. If this is called more than once
    it shouldn't drop the existing tables
    """

    metadata = schema.MetaData()

    appcategory_table = schema.Table(CATEGORY_TABLE, metadata,
                                     schema.Column('package_name', types.VARCHAR(30), primary_key=True),
                                     schema.Column('package_category', types.TEXT(), default=u''),
                                     )


    metadata.bind = engine
    metadata.create_all(checkfirst= True)