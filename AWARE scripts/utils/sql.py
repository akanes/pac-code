from sqlalchemy import text
from config import  *
"""
    Handles all queries from the database. All calls
"""




GET_DEVICES = "SELECT distinct device_id FROM {0} ORDER BY device_id;"

GET_IOS_DEVICES = text("SELECT distinct device_id FROM aware_device WHERE manufacturer = 'Apple' AND label LIKE '%BC%'")
GET_ANDROID_DEVICES = text("SELECT distinct device_id "\
                      "FROM aware_device "\
                      "WHERE manufacturer != 'Apple' "\
                      "AND label LIKE '%BC%'")


LIGHT_TABLE = "light"
GET_LIGHT = "SELECT * FROM 'light' WHERE 'timestamp' BETWEEN %s and %s"
GET_LIGHT_USER = "SELECT * FROM light WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

GET_NOISE_ENERGY = "SELECT * FROM 'light' WHERE 'timestamp' BETWEEN %s and %s"
GET_NOISE_ENERGY_USER = "SELECT * FROM light WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"


ACTIVITY_TABLE = "plugin_google_activity_recognition"
GET_ACTIVITY_USER = "SELECT * FROM " +  ACTIVITY_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

SCREEN_TABLE = "screen"
GET_SCREEN_USER = "SELECT * FROM " + SCREEN_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

BATTERY_TABLE = "battery_charges"
GET_BATTERY_USER = "SELECT * FROM " +  BATTERY_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

CATEGORY_TABLE = "application_categories"
GET_ALL_CATEGORIES = "SELECT * FROM " + CATEGORY_TABLE
GET_CATEGORY = "SELECT * FORM " + CATEGORY_TABLE + " WHERE app_id = {0}"
MAKE_CATEGORY_TABLE = text("CREATE TABLE `"+ CATEGORY_TABLE + "` ("\
  "`package_category` text, "\
  "`package_name` varchar(50) DEFAULT NULL,"\
  "KEY `package_name` (`package_name`) "\
") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1 "\
";")



APP_TABLE = "applications_history"
GET_APP_USER =  "SELECT * FROM " +  APP_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}' AND is_system_app = 0"

GET_UN_UPDATED_IDS = "SELECT DISTINCT(app.package_name) as 'package_name' " \
                     "FROM " + APP_TABLE + " app " \
                     "LEFT JOIN " + CATEGORY_TABLE + " cat ON cat.package_name = app.package_name " \
                     "WHERE cat.package_name IS NULL AND app.is_system_app = 0"



GET_APPS_WITH_CATEGORIES_USER = "SELECT timestamp, app.device_id, app.package_name, cat.package_category, app.process_importance, app.double_end_timestamp " \
                                "FROM " +  APP_TABLE + " as app " \
                                "LEFT JOIN " + CATEGORY_TABLE + " as cat ON app.package_name = cat.package_name " +\
                                " WHERE (app.timestamp BETWEEN {0} AND {1}) AND app.device_id = '{2}' AND app.is_system_app = 0"

GET_MOST_COMMON_USER = "SELECT device_id FROM {0} GROUP BY device_id ORDER BY COUNT(device_id) DESC LIMIT 1"

MESSAGE_TABLE = "messages"
GET_MESSAGES_USER  = "SELECT timestamp, message_type, trace FROM " + MESSAGE_TABLE +  " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

CALL_TABLE = "calls"
GET_CALLS_USER = "SELECT timestamp, call_type, call_duration, trace FROM " + CALL_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

KEYBOARD_TABLE = "keyboard"
GET_KEYBOARD_USER = "SELECT timestamp, before_text, current_text FROM " + KEYBOARD_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

CONVERSATION_TABLE_ANDROID = "plugin_studentlife_audio_android"
GET_CONVERSATION_ANDROID = "SELECT timestamp, double_energy as 'energy', convo_start, convo_end FROM " \
                            + CONVERSATION_TABLE_ANDROID + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

CONVERSATION_TABLE_IOS = "plugin_studentlife_audio"
GET_CONVERSATION_IOS = "SELECT timestamp, double_energy as 'energy', int_convo_start as 'convo_start' , int_convo_end as 'convo_end'  FROM " \
                    + CONVERSATION_TABLE_IOS + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

BLUETOOTH_TABLE = "bluetooth"
GET_BLUETOOTH_USER = "SELECT timestamp, device_id, bt_address FROM " + BLUETOOTH_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

#This is a very, very big table
WIFI_TABLE = "wifi"
GET_WIFI_USER = "SELECT timestamp, bssid FROM " + WIFI_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

GPS_TABLE = "locations"
GET_GPS_USER = "SELECT timestamp, double_latitude as 'latitude', double_longitude as 'longitude'  " \
                "FROM " + GPS_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

ESM_TABLE = "esms"
GET_ESM_USER = "SELECT DISTINCT device_id, timestamp, esm_title, esm_type, esm_instructions, esm_radios, esm_user_answer, double_esm_user_answer_timestamp"\
               " FROM " + ESM_TABLE + ""\
               " WHERE (timestamp BETWEEN {0} AND {1})"\
               " AND device_id = '{2}'"\
               " AND esm_user_answer!=''"\
               " AND esm_title NOT LIKE '[test]%%';"

COMPLETED_TABLE = "completed_analysis"
COMPLETED_USER = "SELECT * FROM completed_analysis WHERE device_id = '{}'"
MAKE_COMPLETED_TABLE= text("CREATE TABLE `" + COMPLETED_TABLE + "` ("\
 " `id` int(11) unsigned NOT NULL AUTO_INCREMENT,"\
 " `device_id` varchar(150) DEFAULT NULL,"\
 " `sensor` varchar(50) DEFAULT NULL,"\
 " `period` varchar(50) DEFAULT NULL,"\
 " `t1` datetime DEFAULT NULL,"\
 " `t2` datetime DEFAULT NULL,"\
 " PRIMARY KEY (`id`)"\
") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1 "\
";")

ACCELEROMETER_TABLE = "accelerometer"
GET_ACCELEROMETER_USER =  "SELECT * FROM " + ACCELEROMETER_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

ROTATION_TABLE = "rotation"
GET_ROTATION_USER = "SELECT * FROM " + ROTATION_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"

TEMPERATURE_TABLE = "temperature"
GET_TEMPERATURE_USER = "SELECT * FROM " + TEMPERATURE_TABLE + " WHERE (timestamp BETWEEN {0} AND {1}) AND device_id = '{2}'"