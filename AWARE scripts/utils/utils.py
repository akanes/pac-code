from pytz import UTC
from pytz import timezone
import datetime as dt
import pandas as pd
from sqlalchemy.types import *
from sql import *



#Weirdly, pd.TimeGrouper doesnt seem to like this business with returning a series - could be sped up
#by a lot if we could get this to work

def fix_times(df, cols=["timestamp"]):
    """Returns the same dataframe with a new datetime column accounting for daylight savings time"""

    for col in cols:

        datetimes = pd.to_datetime(df[col], unit="ms")
        eastern = timezone('US/Eastern')
        datetimes_eastern = datetimes.apply(lambda x: x.tz_localize(UTC).astimezone(eastern))
        if col == "timestamp":
            name = "datetime_EST"
        else:
            name = col + "_EST"
        df[name] = datetimes_eastern

    return df

def groupby_five_minute(timestamp):
    """A function that can be passed to groupby to group a df or
    series into five minute intervals"""
    # return (df["timestamp"] / (5 * 60 * 1000)).apply( lambda x : int(x) * (5 * 60 * 1000))
    return timestamp - dt.timedelta(minutes= (timestamp.minute %5),
                                    seconds = timestamp.second,
                                    microseconds = timestamp.microsecond)

def groupby_one_hour(timestamp):
    return timestamp - dt.timedelta(minutes=timestamp.minute,
                                    seconds=timestamp.second,
                                    microseconds=timestamp.microsecond)

def groupby_half_hour(timestamp):
    return timestamp - dt.timedelta(minutes=timestamp.minute % 30,
                                    seconds=timestamp.second,
                                    microseconds=timestamp.microsecond)
def _get_segment(timestamp):

    hour = (timestamp.hour / 4) * 4
    day = timestamp.day
    month = timestamp.month
    year = timestamp.year

    return dt.datetime(year,month,day, hour = hour)


def groupby_segment(timestamp):
    """A function that can be passed to groupby to group a df into time interval"""
    hour_group = (timestamp.hour / 4) * 4
    hour = timestamp.hour
    minute = timestamp.minute
    second = timestamp.second
    ms = timestamp.microsecond

    return timestamp - dt.timedelta(hours= (hour - hour_group),
                                    minutes = minute,
                                    seconds= second,
                                    microseconds = ms)

def groupby_day(timestamp):
    return timestamp - dt.timedelta(hours = timestamp.hour,
                                    minutes= timestamp.minute,
                                    seconds = timestamp.second,
                                    microseconds = timestamp.microsecond)

def groupby_week(timestamp):
    #Note that weekday 1 is Monday, weekday 7 is sunday
    return timestamp - dt.timedelta(days = timestamp.isoweekday() %7,
                                    hours = timestamp.hour,
                                    minutes=timestamp.minute,
                                    seconds=timestamp.second,
                                    microseconds=timestamp.microsecond)
GROUPING_FUNCTIONS = {
    "five_minute" : groupby_five_minute,
    "segment"     : groupby_segment,
    "day"         : groupby_day,
    "week"        : groupby_week,
    "one_hour"    : groupby_one_hour,
    "half_hour"   : groupby_half_hour
}


def apply_features_fn(fns):
    '''Returns a function that can be used with group.apply to calculate features'''
    fn = lambda g:  pd.Series({fn.__name__ : fn(g) for fn in fns})
    return fn

def all_groups(df,fns, device_id = None):
    """Returns a dict of the features in fns aggregated by all four types
       Dict is organized:
        {
            "five_minute" : df,
            "segment"     : df,
            "day"         : df,
            "week"        :df
        }

        If user is not none, add a column of that device_id to each dataframe
    """
    grouped_features = {}
    for key, groupfn in GROUPING_FUNCTIONS.iteritems():
        grouped = df.groupby(groupfn)
        feature_generator  = apply_features_fn(fns)
        features = grouped.apply(feature_generator)

        if device_id:
            features["device_id"] = pd.Series([device_id] * len(features), index = features.index)
            old_columns = features.columns.tolist()
            new_cols =  old_columns[-1:] + old_columns[:-1]
            features = features[new_cols]

        grouped_features[key] = features

    return grouped_features

def fetch_data(device_id, getter ,conn, t1, t2):
    """

    :param device_id: device_id to be fetched
    :param getter: a 'getter' string from sql.py
    :param conn: sqlalchemy connection
    :return: data with datetime_EST column

    """
    data = pd.read_sql(getter.format(t1, t2, device_id), conn)
    if data.empty:
        #No records
        print "None found!"
        print getter.format(t1, t2, device_id)
        return  None
    data = fix_times(data).set_index("datetime_EST")
    return  data


def dict_to_sql(df_dict, sensor_name, types, conn):

    """
    Send a dictionary of dfs to the database
    :param df_dict: dict of data
    :param sensor_name: the sensor that generated the data
    :param types: a dict relating the column names of the functions to sql types
    :param conn: a sqlalchemy connection
    """

    types["datetime_EST"] = DateTime(timezone=True)
    types["ID"] = Integer

    if "device_id" in df_dict.values()[0].columns.tolist():
        types["device_id"] = String(length=150)

    for key, df in df_dict.iteritems():
        df_to_sql(df,key,sensor_name,types,conn)

def df_to_sql(df,key,sensor_name, types, conn, resample = True):
    """

    :param df: The df to be sent to the database
    :param key: a grouping duration (eg 'week' or 'day')
    :param sensor_name: a sensor name
    :param types: a dictionary linking each column in df to a sqlalchemy datatype
    :param conn: an sqlalchemy connection
    :param resample: whether or not to reindex the df so that periods without data are included
    :return: None

    """

    table = "_" + sensor_name + "_" + key

    device_id = df["device_id"].iloc[0]
    #Resample if desired, unless it's wee
    if resample:
        # df  = df.resample(KEYS_TO_RESAMPLE[key]).reindex()
        df = df.reindex(pd.date_range(start=df.index.min(), end=df.index.max(), freq=KEYS_TO_RESAMPLE[key]))
        df["device_id"] = df["device_id"].fillna(device_id)
    # Check if the table exists:
    check = conn.execute("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{}'".format(table))
    if check.first() is None:
        df["ID"] = pd.Series(range(len(df)), index=df.index)
    else:
        df['ID'] = pd.read_sql_query('select ifnull(max(id),0)+1 from {}'.format(table), conn).iloc[0, 0] + range(
            len(df))


    #Generate datetime column from the index
    df["datetime_EST"] = pd.Series(df.index.to_pydatetime(), index=df.index).map(
        lambda x: x.strftime("%Y-%m-%d %H:%M:%S%z"))

    df.to_sql(table, conn, if_exists="append", dtype=types, index=False)

    # Confirm the insertion by updating the completed_analysis table
    complete = {"device_id": [df["device_id"].iloc[0]],
                "t1": [df.index.min().to_pydatetime().strftime("%Y-%m-%d %H:%M:%S%z")],
                "t2": [df.index.max().to_pydatetime().strftime("%Y-%m-%d %H:%M:%S%z")],
                "sensor": [sensor_name],
                "period": [key]
                }
    pd.DataFrame(complete).to_sql(COMPLETED_TABLE, conn, if_exists="append", index=False)
    print "Saved {0} {1}  with {2}  records for user {3} to database".format(key, sensor_name, len(df),
                                                                             device_id)

#Converts the time period key to a pandas-compatible resampling key
KEYS_TO_RESAMPLE = { "day" : "1D",
                     "five_minute" : "5min",
                     "one_hour"    : "1h",
                     "half_hour"   : "30min",
                     "week" : "1W",
                     "segment" : "4h"}