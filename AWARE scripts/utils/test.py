import pandas as pd
from sql import *
from utils import  *
from sqlalchemy import create_engine

engine = create_engine("mysql://{0}:{1}@{2}/{3}".format(USER, PASSWORD, HOST, DBNAME))
connection = engine.connect()

def get_largest_user_data(table, limit = 5):
    sql = GET_MOST_COMMON_USER.format(table)

    table_data = pd.read_sql(GET_MOST_COMMON_USER.format(table),connection)
    # table_data = pd.read_sql("SELECT device_id FROM applications_history GROUP BY device_id ORDER BY COUNT(device_id) DESC LIMIT 1",connection)
    most_common = table_data["device_id"].values[0]


    most_common_query  ="SELECT * FROM {0} WHERE device_id = '{1}'  LIMIT {2}".format(table, most_common, limit)
    print most_common_query

    return fix_times(pd.read_sql(most_common_query,connection)).set_index("datetime_EST")



def get_test_apps(limit = 5):
    return get_largest_user_data(APP_TABLE, limit= limit)

def get_test_light(limit = 5):
    return  get_largest_user_data(LIGHT_TABLE, limit= limit)

def get_test_gps(limit = 5):
    return get_largest_user_data(GPS_TABLE, limit= limit)

def get_test_activities(limit = 5):
    return get_largest_user_data(ACTIVITY_TABLE,limit= limit)

def get_test_battery(limit = 5):
    return get_largest_user_data(BATTERY_TABLE, limit= limit)

def get_test_bluetooth(limit = 5):
    return get_largest_user_data(BLUETOOTH_TABLE,limit= limit)

def get_test_conversations(limit = 5):
    return get_largest_user_data(CONVERSATION_TABLE_ANDROID,limit= limit)

def get_test_keyboard(limit = 5):
    return get_largest_user_data(KEYBOARD_TABLE, limit = limit)

def get_test_light(limit = 5):
    return get_largest_user_data(LIGHT_TABLE,limit= limit)

def get_test_gps(limit = 5):
    return get_largest_user_data(GPS_TABLE, limit= limit)

def get_test_screen(limit = 5):
    return get_largest_user_data(SCREEN_TABLE, limit=limit)

def get_test_messages(limit =5):
    return get_largest_user_data(MESSAGE_TABLE, limit=limit)

def get_test_esm(limit = 5):
    return get_largest_user_data(ESM_TABLE, limit= limit)