##About
This module exists to extract features from AWARE databases. The full documentation for the AWARE framework can be found at: <a href = 'http://www.awareframework.com/'>http://www.awareframework.com/</a>.
Much of this module is not configured to work "out of the box". It should not be thought of as a standalone program, rather as a framework for generating features from your AWARE database. It provides some functionality for creating tables and generating features, but in most cases will not work without some configuration.

##General structure
Analysis in this module is broken up into individual scripts for each sensor stream. Currently this module does not nicely support multi-modal feature extraction. The currently supported sensor streams are:

*activity recognition
*app use
*battery
*bluetooth
*phone calls
*conversations (from StudentLife plugin)
*ESM (see script for caveats)
*keyboard
*light
*screen
*wifi (see script for caveats)
*message

Inside each of these scripts you will find a function for updating the sensor stream, and a series of functions for calculating features. These functions can be imported into another script if you wish to write your own feature extraction routines.

Analysis is conducted by an update_x function in each script, where x is the name of the script. For convenience, each of these functions takes the same arguments. In each of these functions data is grouped, and features are calculated, indexed, and sent to the server. Each script creates and maintains feature tables in the database.

##Setup
As this is not an "out of the box" program there are several options that you may have to consider when you run this set of scripts on your server.

#utils/config.py
This will almost always need to be edited. It contains the server login credentials that you will need to supply in order access your database.

#__main__.py
The main thing you will want to change in this file are the parameters t1 and t2. These will dictate the range of time that the analysis will be completed over. They should be provided in unix millisecond timestamps.

This is also where you can select which sensor modules you want to run on your data. This is done by adding or removing update_x functions from "func_dict"

#utils/sql.py
All sql queries belong to this file. Before running the program, look through this file. Do all queried tables match the database? If they're AWARE-generated then they should. Column names should also match, but it's worth checking over.

One common thing that may need to be changed is how users are selected for analysis. Many AWARE studies include dummy users that are not actually part of the study. As a default, the SQL queries for users are split up so that only users with label like %BC% are selected.

#utils/utils.py
If you need to adjust the time groups that are created you'll want to do that here. You can easily define custom grouping functions and add/remove them from GROUPING_FUNCTIONS

##Running
To run the script (once configured) return to the enclosing directory and simply execute:
    python CMU_features

##Requirements
To install requirements run the following command from inside CMU_features:
    pip install -r requirements.txt

##Extending the module

#Adding a sensor
To add a sensor, create a new module matches the general format of one of the existing ones. The easiest one to understand is probably light.py.