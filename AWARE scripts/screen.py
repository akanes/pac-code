from utils.sql import GET_DEVICES, GET_SCREEN_USER, SCREEN_TABLE
from utils.utils import  *
from sqlalchemy.types import *

def update_screen(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """
    screen_data = fetch_data(device_id,GET_SCREEN_USER,conn,t1,t2)
    if screen_data is None:
        return  pd.DataFrame()
    results = all_groups(screen_data,SCREEN_APPLY, device_id=device_id)
    dict_to_sql(results, "screen", SCREEN_SQL_TYPES, conn)


def unlocks_per_minute(g):
    #Is this right? What about missing data?
    count = g[g["screen_status"] == 3]["screen_status"].count()
    minutes = (g.index.max() - g.index.min()).total_seconds() / 60.
    if minutes == 0:
        return 0
    else:
        return  count / minutes

def duration_interaction_seconds(g):
    """This is a little hard to implement because there aren't always clean gaps between interactions,
    we might see:
        t1 on
        t2 unlock
        t3 unlock
        t4 lock
        t5 off
    Which makes calculating duration ambiguous. To remedy, we'll measure the duration of events that
    start with an unlock and end with either screen off or lock, as (no sure about this) screen on
    could be triggered by a notification
    """

    selector = ((g["screen_status"] == 3) & (g["screen_status"].shift(-1).isin([1,2])))
    # print selector
    selected = g[selector]
    return int((g["timestamp"].shift(-1) -  g["timestamp"])[selector].sum() / 1000)

SCREEN_APPLY = [
    unlocks_per_minute,
    duration_interaction_seconds
]

SCREEN_SQL_TYPES ={
    "unlocks_per_minute" : Float,
    "duration_interaction_seconds" : Integer
}




