DELETE FROM _activity_day;
DELETE FROM _activity_five_minute;
DELETE FROM _activity_segment;
DELETE FROM _activity_week;

DELETE FROM _app_day;
DELETE FROM _app_five_minute;
DELETE FROM _app_segment;
DELETE FROM _app_week;

DELETE FROM _battery_day;
DELETE FROM _battery_five_minute;
DELETE FROM _battery_segment;
DELETE FROM _battery_week; 

DELETE FROM _bluetooth_day;
DELETE FROM _bluetooth_five_minute;
DELETE FROM _bluetooth_segment;
DELETE FROM _bluetooth_week; 

DELETE FROM _call_day;
DELETE FROM _call_five_minute;
DELETE FROM _call_segment;
DELETE FROM _call_week; 

DELETE FROM _conversation_day;
DELETE FROM _conversation_five_minute;
DELETE FROM _conversation_segment;
DELETE FROM _conversation_week; 

DELETE FROM _keyboard_day;
DELETE FROM _keyboard_five_minute;
DELETE FROM _keyboard_segment;
DELETE FROM _keyboard_week;

DELETE FROM _light_day;
DELETE FROM _light_five_minute;
DELETE FROM _light_segment;
DELETE FROM _light_week;

DELETE FROM _message_day;
DELETE FROM _message_five_minute;
DELETE FROM _message_segment;
DELETE FROM _message_week;

DELETE FROM _screen_day;
DELETE FROM _screen_five_minute;
DELETE FROM _screen_segment;
DELETE FROM _screen_week;

-- DELETE FROM __day;
-- DELETE FROM __five_minute;
-- DELETE FROM __segment;
-- DELETE FROM __week;


DELETE FROM completed_analysis