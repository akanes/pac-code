from utils.utils import fetch_data, all_groups, dict_to_sql
from utils.sql import GET_WIFI_USER
import pandas as pd
from sqlalchemy.types import *

def update_wifi(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None

    Note: In some databases wifi data can be dozens of millions of lines.
    This script has not been tested in these cases.
    """

    data = fetch_data(device_id, GET_WIFI_USER, conn, t1, t2)
    if data is None:
        return  pd.DataFrame()
    results = all_groups(data,WIFI_APPLY, device_id=device_id)
    dict_to_sql(results, "wifi", WIFI_SQL_TYPES, conn)

def number_unique_wifi_hotspots(g):
    return g["bssid"].nunique()

def most_frequent_wifi(g):
    if g.empty:
        return None
    result = g["bssid"].mode()
    if result.empty:
        return g["bssid"].iloc[0]
    else:
        return result[0]

WIFI_APPLY = [
    number_unique_wifi_hotspots,
    most_frequent_wifi
]

WIFI_SQL_TYPES = {
    "number_unique_wifi_hotspots" : Integer,
    "most_frequent_wifi" : String
}