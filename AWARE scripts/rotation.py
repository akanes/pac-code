from utils.utils import *
from utils.sql import GET_ROTATION_USER
import numpy as np
def update_rotation(t1, t2, conn, device_id, type):

    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    rot_data = fetch_data(device_id,GET_ROTATION_USER,conn,t1,t2)


    #If there is no record for this user:
    if rot_data is None:
        return

    #Add magnitude column
    rot_values = rot_data[["double_values_0","double_values_1","double_values_2"]].values
    rot_data["magnitude"] = np.linalg.norm(rot_values, axis =1)
    results = all_groups(rot_data, ROTATION_FUNCTIONS, device_id=device_id)
    dict_to_sql(results, "rotation",ROTATION_SQL_TYPES, conn)


def mean_magnitude_rotation(g):
    return np.mean(g.magnitude)

def std_magnitude_rotation(g):
    return np.std(g.magnitude)

def median_magnitude_rotation(g):
    return np.median(g.magnitude)

def min_magnitude_rotation(g):
    return np.min(g.magnitude)

def max_magnitude_rotation(g):
    return  np.max(g.magnitude)

ROTATION_FUNCTIONS = [
    mean_magnitude_rotation,
    std_magnitude_rotation,
    median_magnitude_rotation,
    min_magnitude_rotation,
    max_magnitude_rotation,
]

ROTATION_SQL_TYPES ={
    "mean_magnitude_rotation" : Float,
    "std_magnitude_rotation"  : Float,
    "median_magnitude_rotation" : Float,
    "min_magnitude_rotation" : Float,
    "max_magnitude_rotation" : Float
}