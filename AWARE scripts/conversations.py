from utils.sql import GET_DEVICES, GET_CONVERSATION_ANDROID, GET_CONVERSATION_IOS
from utils.utils import fix_times, all_groups, fetch_data, dict_to_sql
import pandas as pd
from sqlalchemy.types import *


#Under the update this will be the new format for fetching data
def update_conversation(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    if type == "ANDROID":
        conversation_data = fetch_data(device_id,GET_CONVERSATION_ANDROID,conn,t1,t2)
    else:
        conversation_data = fetch_data(device_id,GET_CONVERSATION_IOS,conn,t1,t2)

    if conversation_data is None:
        return

    results = all_groups(conversation_data,CONVERSATION_APPLY, device_id = device_id)
    dict_to_sql(results, "conversation", CONVERSATION_SQL_TYPES, conn)

def number_of_conversations(g):
    return g[g["convo_start"] != 0]["convo_start"].count()

def length_of_conversations_seconds(g):
    wconvo= g[g["convo_start"] != 0]
    return int((wconvo["convo_end"] - wconvo["convo_start"]).sum())

def mean_voice_energy(g):
    return g["energy"].mean()

CONVERSATION_APPLY = [
    number_of_conversations,
    length_of_conversations_seconds,
    mean_voice_energy
]

CONVERSATION_SQL_TYPES = {
    "number_of_conversations" : Integer,
    "length_of_conversations_seconds" : Integer,
    "mean_voice_energy" : Float
}
