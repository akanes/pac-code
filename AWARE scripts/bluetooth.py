from utils.utils import fetch_data, all_groups, dict_to_sql
from utils.sql import GET_BLUETOOTH_USER
import pandas as pd
from sqlalchemy.types import *

def update_bluetooth(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    data = fetch_data(device_id, GET_BLUETOOTH_USER, conn, t1, t2)
    if data is None:
        return
    results = all_groups(data,BLUETOOTH_APPLY, device_id = device_id)
    dict_to_sql(results, "bluetooth", BLUETOOTH_SQL_TYPES, conn)

def number_unique_devices(g):
    return g["bt_address"].nunique()

def most_frequent_device(g):
    if g.empty:
        return None
    result = g["bt_address"].mode()
    if result.empty:
        return g["bt_address"].iloc[0]
    else:
        return result[0]


BLUETOOTH_APPLY = [
    most_frequent_device,
    number_unique_devices
]

BLUETOOTH_SQL_TYPES = {
    "most_frequent_device" : String(length = 50),
    "number_unique_devices": Integer
}