from utils.sql import *
from utils.config import *
from sqlalchemy import create_engine
import pandas as pd
import logging
from collections import OrderedDict
import thread
from activity import update_activity
from app import update_app_categories, update_app
from accelerometer import update_accelerometer
from battery import update_battery
from bluetooth import update_bluetooth
from call import update_call
from conversations import update_conversation
from keyboard import update_keyboard
from light import update_light
from location import update_location
from message import update_message
from rotation import update_rotation
from screen import update_screen
from temperature import update_temperature
from esm import update_esm
from wifi import update_wifi


def main():
    #Need to make this
    ERROR_FILENAME = 'CMU_features/logs/errors.out'
    logging.basicConfig(filename=ERROR_FILENAME, level=logging.DEBUG, )

    engine = create_engine("mysql://{0}:{1}@{2}/{3}".format(USER, PASSWORD, HOST, DBNAME))
    connection = engine.connect()

    #Check if the completed table exists:
    if not engine.dialect.has_table(connection, COMPLETED_TABLE):
        engine.execute(MAKE_COMPLETED_TABLE)

    #Check if app category table exists:
    if not engine.dialect.has_table(connection, CATEGORY_TABLE):
        engine.execute(MAKE_CATEGORY_TABLE)

    android = pd.read_sql(GET_ANDROID_DEVICES, connection)["device_id"].tolist()
    ios = pd.read_sql(GET_IOS_DEVICES, connection)["device_id"].tolist()
    types = (["ANDROID"] * len(android)) + (["IOS"] * len(ios))

    #The New Year
    t1 = 1451624400000
    #July 26 2016
    t2 = 1469550456000
    times1 = [t1] * len(types)
    times2 = [t2] * len(types)


    cons = [connection] * len(types)

    update_args = zip(times1, times2, cons, android + ios , types)

    for device in update_args:
        try:
            update_all(*device)
        except:
            logging.exception("Got an error on".format(*device[3]))

    connection.close()

def update_all(t1,t2,conn,device_id, type):
    #See what's been done already:
    comp = pd.read_sql(COMPLETED_USER.format(device_id), conn)
    for flag, func in UPDATE_FUNCTIONS.iteritems():
        if not flag in comp["sensor"].values:
            print "Calculating {0} for user: {1}".format(flag, device_id)
            func(t1,t2,conn,device_id, type)
        else:
            print  "Skipping {0} for user {1}".format(flag, device_id)

func_dict = {
    "accelerometer" : update_accelerometer,
    "activity" : update_activity,
    "app" : update_app,
    "battery" : update_battery,
    "bluetooth" : update_bluetooth,
    "call" : update_call,
    "conversation" : update_conversation,
    "esm" : update_esm,
    "keyboard" : update_keyboard,
    "light" : update_light,
    "location" : update_location,
    "message" : update_message,
    "rotation" : update_rotation,
    "screen" : update_screen,
    "temperature" : update_temperature,
    "wifi" :update_wifi
 }

UPDATE_FUNCTIONS = OrderedDict(sorted(func_dict.items(), key=lambda t: t[0]))


if __name__ == "__main__":
    #Can we multi-thread this? Data structures are all independent, will the server be able to handle it?
    #Before adding data need to drop all between the time stamps
    #Whats a reasonable duration to resample to?
    #Definitely want to resample every df before sending to SQL

    main()
