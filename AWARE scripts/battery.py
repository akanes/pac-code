import pandas as pd
import numpy as np
from utils.sql import GET_BATTERY_USER, GET_DEVICES, BATTERY_TABLE
from utils.utils import *
from sqlalchemy.types import *

def update_battery(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    battery_data = fetch_data(device_id,GET_BATTERY_USER,conn,t1,t2)
    if battery_data is None:
        return
    results = all_groups(battery_data,BATTERY_APPLY, device_id = device_id)
    dict_to_sql(results, "battery", BATTERY_SQL_TYPES, conn)

def length_of_charge_seconds(g):
    """Returns the total charge duration in a time period"""
    return int(((g["double_end_timestamp"] - g["timestamp"]) ).sum() / 1000)

BATTERY_APPLY = [
    length_of_charge_seconds
]

BATTERY_SQL_TYPES = {
    "length_of_charge_seconds" : Integer
}
