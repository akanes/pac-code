import pandas as pd
import numpy as np
from utils.sql import GET_LIGHT_USER, GET_DEVICES, LIGHT_TABLE
from utils.utils import *
from sqlalchemy.types import *


def update_light(t1, t2, conn, device_id, type):

    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    light_data = fetch_data(device_id,GET_LIGHT_USER,conn,t1,t2)


    #If there is no record for this user:
    if light_data is None:
        return
    results = all_groups(light_data,LIGHT_FUNCTIONS, device_id=device_id)
    dict_to_sql(results, "light",LIGHT_SQL_TYPES, conn)


def mean_lux(g):
    return np.mean(g.double_light_lux)

def std_lux(g):
    return np.std(g.double_light_lux)

def median_lux(g):
    return np.median(g.double_light_lux)

def min_lux(g):
    return np.min(g.double_light_lux)

def max_lux(g):
    return  np.max(g.double_light_lux)

LIGHT_FUNCTIONS = [
    mean_lux,
    std_lux,
    median_lux,
    min_lux,
    max_lux,
]

LIGHT_SQL_TYPES ={
    "mean_lux" : Float,
    "std_lux"  : Float,
    "median_lux" : Float,
    "min_lux" : Float,
    "max_lux" : Float
}