from utils.sql import GET_DEVICES, GET_CALLS_USER, CALL_TABLE
from utils.utils import fix_times, all_groups, fetch_data, dict_to_sql
import pandas as pd
from sqlalchemy.types import *

def update_call(t1, t2, conn, device_id, type):
    """
    :param t1: A unix timestamp (in ms) indicating the start of analysis
    :param t2: A unix timestamp (in ms) indicating the end of analysis
    :param conn: A SQLAlchemy Connection object
    :param device_id: A device_id for which the analysis will be run
    :param type: one of either IOS or ANDROID
    :return: None
    """

    call_data = fetch_data(device_id,GET_CALLS_USER,conn,t1,t2)
    if call_data is None:
        return

    results = all_groups(call_data, CALL_APPLY, device_id = device_id)
    dict_to_sql(results, "call", CALL_SQL_TYPES, conn)



#Not sure if it's better to combine these to speed things up, or to
#Leave it this way to make use of the modularity

def number_outgoing_calls(g):
    return g[g["call_type"] == 2]["call_type"].count()

def number_incoming_calls(g):
    return  g[g["call_type"] == 1]["call_type"].count()

def number_missed_calls(g):
    return  g[g["call_type"] == 3]["call_type"].count()

def duration_outgoing_calls_seconds(g):
    return  int(g[g["call_type"] == 2]["call_duration"].sum())

def duration_incoming_calls_seconds(g):
    return  int(g[g["call_type"] == 1]["call_duration"].sum())

def most_frequent_correspondent_phone(g):
    if g.empty:
        return None
    result = g["trace"].mode()
    if result.empty:
        return g["trace"].iloc[0]
    else:
        return result[0]

def number_of_correspondents_phone(g):
    return g["trace"].nunique()


CALL_APPLY = [
    number_incoming_calls,
    number_outgoing_calls,
    number_missed_calls,
    duration_incoming_calls_seconds,
    duration_outgoing_calls_seconds,
    most_frequent_correspondent_phone,
    number_of_correspondents_phone
]

CALL_SQL_TYPES = {
    "number_incoming_calls" : Integer,
    "number_outgoing_call" : Integer,
    "number_missed_calls" : Integer,
    "duration_incoming_calls_seconds": Integer,
    "duration_outgoing_calls_seconds": Integer,
    "most_frequent_correspondent" : Text,
    "number_of_correspondents" : Integer
}
