__author__ = 'michaelmerrill'

import urllib2
from bs4 import BeautifulSoup
import csv
import ast
import time

crawled = set()


def GetAppCategory(appid):
    url = "https://play.google.com/store/apps/details?id=" + appid
    try:
        response = urllib2.urlopen(url)
    except urllib2.HTTPError:
        return "None"

    soup = BeautifulSoup(response, 'html.parser')
    text = soup.find('span', itemprop='genre')

    if text:
        return text.contents[0]
    else:
        return "NoneOrUtility"


root = "/Users/michaelmerrill/Desktop/Summer_Work/EUREKA/df"

USER_IDS = ['u007', 'u008', 'u009', 'u010', 'u011', 'u012', 'u013', 'u014', 'u017', 'u018', 'u022', 'u023']

with open("app_catergories.csv", "w") as app_categories:
    to_write = csv.writer(app_categories)
    for user in USER_IDS:
        print "Searching for user: " + user
        with open(root + "/" + user + "/" + user + "_app_lists.csv") as user_apps:
            isFirst = True
            reader = csv.reader(user_apps)
            for line in reader:
                if not isFirst:
                    apps = ast.literal_eval(line[1])
                    for app in apps:
                        if app in crawled:
                            continue
                        else:
                            category = GetAppCategory(app)
                            new_line = [app,category]
                            to_write.writerows(new_line)
                            crawled.add(app)
                            print(new_line)
                            print len(crawled)
                            time.sleep(3)


                isFirst = False
