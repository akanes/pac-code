'''
 * Created by ruiwang on 11/30/16
'''

import pymongo
from operator import itemgetter
from datetime import timedelta
import numpy
from util.events_util import EventsUtil


# android
## get unlock events android
def get_lock_events_android(db, uid, start_time, end_time):
    # get data 2 hour before and after
    time_padding = 2
    lock_events = list(db['bio_events_100'].find({'uid': uid,
                                                  'event_time': {'$gte': start_time + timedelta(hours=-time_padding),
                                                                 '$lt': end_time + timedelta(
                                                                     hours=time_padding)}}).sort('event_time',
                                                                                                 pymongo.ASCENDING))

    lock_list = []

    for item in lock_events:
        try:
            event_time = item['event_time']
            lock_stat = int(item['event_data'])
            lock_list.append([event_time, lock_stat])
        except:
            pass

    event_list = generate_unlock_events(lock_list)
    sample_val = generate_unlock_vector(event_list, start_time, end_time)

    return event_list, sample_val


## use screen off to generate screen off events and vector
def get_screenon_android(db, uid, start_time, end_time):
    eventUtil = EventsUtil()
    sample_val = numpy.zeros(int((end_time - start_time).total_seconds()))

    screenon_list = list(db['bio_events_15'].find({'uid': uid,
                                                   'event_time': {'$gte': start_time + timedelta(hours=-3),
                                                                  '$lt': end_time}}))
    event_list = []
    for event in screenon_list:
        duration = float(eventUtil.to_Double(event['event_data']))

        event_start_time = event['event_time']
        event_end_time = event_start_time + timedelta(seconds=duration)

        if event_start_time < start_time:
            event_start_time = start_time
        if event_end_time > end_time:
            event_end_time = end_time

        if event_start_time >= event_end_time:
            continue

        event_list.append([event_start_time, event_end_time])

        start_idx = int((event_start_time - start_time).total_seconds())
        end_idx = int((event_end_time - start_time).total_seconds())

        sample_val[start_idx:end_idx] = 1

    return event_list, sample_val


# ios
def get_lock_events_ios(db, uid, start_time, end_time):
    # get data 2 hour before and after
    time_padding = 2
    lock_events = list(db['ios_lock'].find({'uid': uid,
                                            'time': {'$gte': start_time + timedelta(hours=-time_padding),
                                                     '$lt': end_time + timedelta(
                                                         hours=time_padding)}}).sort('time', pymongo.ASCENDING))

    lock_list = []

    for item in lock_events:
        try:
            event_time = item['time']
            # 1 is lock in iOS
            lock_stat = 1 - int(item['LockState'])

            if len(lock_list) > 0 and lock_list[-1][0] == event_time and lock_list[-1][1] == 1:
                lock_list[-1][1] = 0
            else:
                lock_list.append([event_time, lock_stat])
        except:
            pass

    event_list = generate_unlock_events(lock_list)
    sample_val = generate_unlock_vector(event_list, start_time, end_time)

    return event_list, sample_val


# generate unlock events
def generate_unlock_events(raw_events):
    screenon_events = []
    unlock_time = None
    current_stat = 0

    for item in raw_events:
        event_time = item[0]
        lock_stat = item[1]

        if lock_stat == 0:
            # screen off
            # ignore if the current stat is 0, otherwise a unlock event is done
            if current_stat != 0:
                screenon_events.append([unlock_time, event_time])
            unlock_time = None
            current_stat = 0
        elif lock_stat == 1:
            # last unlock event is not finished, may be due to data loss. add screen on for 30s
            if current_stat != 0:
                last_lock_time = unlock_time + timedelta(seconds=30)
                if last_lock_time > event_time:
                    last_lock_time = event_time + timedelta(microseconds=-100)
                if last_lock_time > unlock_time:
                    screenon_events.append([unlock_time, event_time])

            # unlocked, set status
            current_stat = 1
            unlock_time = event_time

    return screenon_events


# generate unlock vector
def generate_unlock_vector(screenon_events, start_time, end_time):
    sample_val = numpy.zeros(int((end_time - start_time).total_seconds()))

    for item in screenon_events:
        start = item[0]
        end = item[1]

        if start < start_time:
            start = start_time
        if end > end_time:
            end = end_time

        if start < end:
            start_idx = int((start - start_time).total_seconds())
            end_idx = int((end - start_time).total_seconds())

            sample_val[start_idx:end_idx] = 1

    return sample_val


# ios
def get_screenon_ios(db, uid, start_time, end_time):
    sample_val = numpy.zeros(int((end_time - start_time).total_seconds()))

    return [], sample_val


# generate features
def generate_unlock_features(unlock_list, day, epoch_def):
    unlock_feat = {}

    # epoch features
    for idx in range(len(epoch_def)):
        start_time = day + timedelta(hours=epoch_def[idx][0])
        end_time = day + timedelta(hours=epoch_def[idx][1])

        num_unlock, length_unlock = get_unlock_stat(unlock_list, start_time, end_time)

        unlock_feat.update(
            {
                'unlock_num_ep_{}'.format(idx): num_unlock,
                'unlock_duration_ep_{}'.format(idx): length_unlock,
            }
        )

    return unlock_feat


def get_unlock_stat(unlock_list, start_time, end_time):
    num_unlock = 0
    length_unlock = 0
    for unlock in unlock_list:
        start = unlock[0]
        end = unlock[1]

        if start < start_time:
            start = start_time
        if end > end_time:
            end = end_time

        if start < end:
            length_unlock = length_unlock + (end - start).total_seconds()
            num_unlock += 1

    return num_unlock, length_unlock
