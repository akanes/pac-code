'''
Created on Oct 29, 2014

@author: ruiwang
'''
from datetime import timedelta
import json

# get call history
import pymongo

from android.data_import import import_utils
from android.data_import.import_funf import CALL_LOG_COLLECTION_NAME, SMS_COLLECTION_NAME
from android.data_import.import_utils import insert_to_db, check_collection
from util import db_util


def get_call_history(db, uid, start_time, end_time):
    call_history_raw = db['calllog'].find(
        {'uid': uid, 'time': {'$gte': start_time, '$lt': end_time}})

    call_log = []
    for item in call_history_raw:
        try:
            call_record = [item['time'], item['duration'], item['type']]
            call_log.append(call_record)
        except:
            pass

    return call_log


# get sms history
def get_sms_history(db, uid, start_time, end_time):
    sms_history_raw = db['smslog'].find(
        {'uid': uid, 'time': {'$gte': start_time, '$lt': end_time}})

    sms_log = []
    for item in sms_history_raw:
        try:
            sms_record = [item['time'], item['msg_type']]
            sms_log.append(sms_record)
        except:
            pass

    return sms_log


# generate call features
def generate_call_features(call_log, day, epoch_def):
    call_feat = {}

    # epoch features
    for idx in range(len(epoch_def)):
        start_time = day + timedelta(hours=epoch_def[idx][0])
        end_time = day + timedelta(hours=epoch_def[idx][1])

        call_num, call_duration = compute_call_feat(call_log, start_time, end_time)

        call_feat.update(
            {
                'call_in_num_ep_{}'.format(idx): call_num[1],
                'call_out_num_ep_{}'.format(idx): call_num[2],
                'call_miss_num_ep_{}'.format(idx): call_num[3],
                'call_in_duration_ep_{}'.format(idx): call_duration[1],
                'call_out_duration_ep_{}'.format(idx): call_duration[2],
            }
        )

    return call_feat


def compute_call_feat(call_log, start_time, end_time):
    '''
    INCOMING_TYPE = 1
    OUTGOING_TYPE = 2
    MISSED_TYPE = 3
    '''
    call_dist = {1: 0, 2: 0, 3: 0}
    call_duration = {1: 0, 2: 0, 3: 0}
    for call in call_log:
        try:
            start = call[0]
            end = start + timedelta(seconds=call[1])

            if start < start_time:
                start = start_time
            if end > end_time:
                end = end_time

            if start < end:
                call_dist[call[2]] += 1
                call_duration[call[2]] += (end - start).total_seconds()
        except:
            pass

    return call_dist, call_duration


# generate sms features
def generate_sms_features(sms_log, day, epoch_def):
    sms_feat = {}

    # epoch features
    for idx in range(len(epoch_def)):
        start_time = day + timedelta(hours=epoch_def[idx][0])
        end_time = day + timedelta(hours=epoch_def[idx][1])

        sms_num = compute_sms_feat(sms_log, start_time, end_time)
        sms_feat.update(
            {
                'sms_in_num_ep_{}'.format(idx): sms_num[1],
                'sms_out_num_ep_{}'.format(idx): sms_num[2]
            }
        )

    return sms_feat


def compute_sms_feat(sms_log, start_time, end_time):
    '''
    MESSAGE_TYPE_INBOX == 1
    MESSAGE_TYPE_SENT == 2
    MESSAGE_TYPE_DRAFT == 3
    '''
    sms_dist = {1: 0, 2: 0, 3: 0}
    for sms in sms_log:
        if sms[0] >= start_time and sms[0] < end_time:
            try:
                sms_dist[sms[1]] += 1
            except:
                pass

    return sms_dist


def convert_call_log(db, cond={}):
    check_collection(db, CALL_LOG_COLLECTION_NAME, [('uid', pymongo.ASCENDING), ('time', pymongo.ASCENDING)])

    call_log_raw = list(db['bio_events_210'].find(cond))

    for item in call_log_raw:
        uid = item['uid']
        try:
            call_json = json.loads(item['event_data'])
            call_list = call_json['CALLS']
            for call_item in call_list:
                try:
                    duration = call_item['duration']
                    type = call_item['type']
                    timestamp = call_item['date'] / 1000
                    call_time = db_util.convert_sqlite3_time(timestamp)
                    number = call_item['number']

                    call_doc = {
                        'uid': uid,
                        'time': call_time,
                        'duration': duration,
                        'type': type,
                        'number': number
                    }

                    insert_to_db(db, CALL_LOG_COLLECTION_NAME, call_doc, import_utils.dryrun)
                except:
                    pass
        except:
            pass


def convert_sms_log(db, cond={}):
    check_collection(db, SMS_COLLECTION_NAME, [('uid', pymongo.ASCENDING), ('time', pymongo.ASCENDING)])

    sms_log_raw = list(db['bio_events_211'].find(cond))

    for item in sms_log_raw:
        uid = item['uid']
        try:
            sms_json = json.loads(item['event_data'])
            sms_list = sms_json['SMS']
            for sms_item in sms_list:
                try:
                    sms_type = sms_item['type']
                    timestamp = sms_item['date'] / 1000
                    sms_time = db_util.convert_sqlite3_time(timestamp)
                    address = sms_item['address']
                    read = sms_item['read']
                    body = sms_item['body']
                    status = sms_item['status']
                    thread_id = sms_item['thread_id']

                    sms_doc = {
                        'uid': uid,
                        'msg_type': sms_type,
                        'time': sms_time,
                        'number': address,
                        'is_read': read,
                        'body_len': body,
                        'status': status,
                        'thread_id': thread_id,
                    }

                    insert_to_db(db, SMS_COLLECTION_NAME, sms_doc, import_utils.dryrun)
                except:
                    pass
        except:
            pass
