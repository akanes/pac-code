import json
import numpy
from datetime import datetime, timedelta
from util.events_util import EventsUtil
import warnings

audio_map_android = {'silence': 0, 'voice': 1, 'noise': 2}
audio_ios2android = {0: 0, 1: 2, 2: 1}


##################
## audio amp features
def generate_audio_amp_features(audio_vector, epoch_def):
    epoch_def_secs = [
        [item[0] * 3600, item[1] * 3600]
        for item in epoch_def
        ]

    # all day feats
    audio_feat = {}

    # epoch features
    for idx in range(len(epoch_def_secs)):
        start_idx = epoch_def_secs[idx][0]
        end_idx = epoch_def_secs[idx][1]

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            audio_amp_mean = numpy.nanmean(audio_vector[start_idx:end_idx])
            audio_amp_std = numpy.nanstd(audio_vector[start_idx:end_idx])

        audio_feat.update(
            {
                'audio_amp_mean_ep_{}'.format(idx): audio_amp_mean,
                'audio_amp_std_ep_{}'.format(idx): audio_amp_std,
            }
        )

    return audio_feat


## audio voice features
def generate_audio_voice_features(audio_vector, epoch_def):
    epoch_def_secs = [
        [item[0] * 3600, item[1] * 3600]
        for item in epoch_def
        ]

    # all day feats
    audio_feat = {}

    # epoch features
    for idx in range(len(epoch_def_secs)):
        start_idx = epoch_def_secs[idx][0]
        end_idx = epoch_def_secs[idx][1]

        num_inference = float(sum(numpy.isfinite(audio_vector[start_idx:end_idx])))
        num_voice = float(sum(audio_vector[start_idx:end_idx] == 1))

        # avoid dividing by zero
        if num_inference == 0:
            num_inference = 1

        audio_feat.update(
            {
                'audio_voice_ep_{}'.format(idx): num_voice / num_inference,
            }
        )

    return audio_feat


## convo
def generate_audio_convo_features(convo_list, day, epoch_def):
    convo_feat = {}

    # epoch features
    for idx in range(len(epoch_def)):
        start_time = day + timedelta(hours=epoch_def[idx][0])
        end_time = day + timedelta(hours=epoch_def[idx][1])

        num_convo, length_convo = get_convo_stat(convo_list, start_time, end_time)

        convo_feat.update(
            {
                'audio_convo_num_ep_{}'.format(idx): num_convo,
                'audio_convo_duration_ep_{}'.format(idx): length_convo,
            }
        )

    return convo_feat


def get_convo_stat(convo_list, start_time, end_time):
    num_convo = 0
    length_convo = 0
    for convo in convo_list:
        start = convo[0]
        end = convo[1]

        if start < start_time:
            start = start_time
        if end > end_time:
            end = end_time

        if start < end:
            length_convo = length_convo + (end - start).total_seconds()
            num_convo += 1

    return num_convo, length_convo


## quality
def get_audio_quality(audio_vector):
    count_thre = 1
    hours = 0;
    for i in range(24):
        start_idx = i * 3600
        end_idx = start_idx + 3600

        count = sum(numpy.isfinite(audio_vector[start_idx:end_idx]))
        if count > count_thre:
            hours += 1

    return hours


##################
# android
## audio amplitude
def get_audio_amplitude_android(db, uid, start_time, end_time):
    eventUtil = EventsUtil()

    event_list = list(db['bio_events_110'].find(
        {'uid': uid, 'event_source': 110, 'event_time': {'$gt': start_time, '$lte': end_time}}))

    audio_readings = [numpy.NaN] * 24 * 3600;

    for item in event_list:
        idx = int((item['event_time'] - start_time).total_seconds())
        try:
            audio_readings[idx] = float(eventUtil.to_Integer(item['event_data']))
        except:
            # print item['event_data']
            pass

    return numpy.array(audio_readings)


## audio inferences
def get_audio_inference_android(db, uid, start_time, end_time):
    eventUtil = EventsUtil()

    event_list = list(db['audio_inferences'].find(
        {'uid': uid, 'time': {'$gt': start_time, '$lte': end_time}}))

    audio_readings = [numpy.NaN] * 24 * 3600;

    for item in event_list:
        idx = int((item['time'] - start_time).total_seconds())
        try:
            audio_readings[idx] = item['type']
        except:
            # print item['event_data']
            pass

    return numpy.array(audio_readings)


## convo
def get_conversation_list_android(db, uid, start_time, end_time):
    # old version from funf
    cond = {
        'uid': uid,
        '$or': [
            {'start': {'$gte': start_time, '$lt': end_time}},
            {'end': {'$gte': start_time, '$lt': end_time}}
        ]
    }

    convo_list = []
    for item in db['raw_conversation'].find(cond):
        try:
            convo_list.append([item['start'], item['end']])
        except:
            # treat as missing values
            pass

    # new version without funf
    if len(convo_list) == 0:
        raw_convo = list(db['bio_events_113'].find({'uid': uid, 'event_time': {'$gte': start_time, '$lt': end_time}}))
        for convo in raw_convo:
            data = convo['event_data']
            j = json.loads(data)
            start = datetime.fromtimestamp(int(j['CONVERSATION_START']) / 1000)
            end = datetime.fromtimestamp(int(j['CONVERSATION_END']) / 1000)

            convo_list.append([start, end])

    return convo_list


##################
# ios
## audio amplitude
def get_audio_amplitude_ios(db, uid, start_time, end_time):
    events_collection = db['ios_audio_level']

    event_list = list(
        events_collection.find({'uid': uid, 'time': {'$gte': start_time, '$lt': end_time}}).sort('time', 1))
    audio_val = [numpy.nan] * int((end_time - start_time).total_seconds())
    for event in event_list:
        time = event['time']
        idx = int((time - start_time).total_seconds())

        try:
            val = float(event['value'])
            audio_val[idx] = val
        except:
            # treat as missing values
            pass

    return numpy.array(audio_val)


## audio inferences
def get_audio_inference_ios(db, uid, start_time, end_time):
    event_list = list(db['ios_audio_voice'].find(
        {'uid': uid, 'time': {'$gt': start_time, '$lte': end_time}}))

    audio_readings = [numpy.NaN] * 24 * 3600;

    for item in event_list:
        idx = int((item['time'] - start_time).total_seconds())
        try:
            audio_readings[idx] = audio_ios2android[item['value']]
        except:
            # treat as missing values
            pass

    return numpy.array(audio_readings)


##convo
def get_conversation_list_ios(db, uid, start_time, end_time):
    convo_collection = db['ios_audio_convo']
    cond = {'uid': uid,
            '$or': [{'start_time': {'$gte': start_time, '$lt': end_time}},
                    {'end_time': {'$gte': start_time, '$lt': end_time}}]}

    convo_list = []
    for item in convo_collection.find(cond):
        try:
            convo_list.append([item['start_time'], item['end_time']])
        except:
            # treat as missing values
            pass

    return convo_list
