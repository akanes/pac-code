from datetime import timedelta

def convert2vector(record, time_getter, start_time, end_time):
    total_sec = int((end_time - start_time).total_seconds())

    vector = [0] * total_sec
    for itm in record:
        item_time = time_getter(itm)
        idx = int((item_time - start_time).total_seconds())
        if idx >= 0 and idx < len(vector):
            vector[idx] = 1

    return vector

def get_data_quality_stat(record, start_time, end_time, time_getter, calc_stat, interval=3600):
    record_vec = convert2vector(record, time_getter, start_time, end_time)

    return eval_data_quality(record_vec, interval, calc_stat)


def eval_data_quality(vector, interval, calc_stat):
    seg_num = int(len(vector) / interval)
    interval_stat = [0] * seg_num

    for i in range(seg_num):
        idx_0 = i * interval;
        idx_1 = (i + 1) * interval;

        if idx_1 > len(vector):
            idx_1 == len(vector)
        if idx_1 < idx_0:
            continue

        interval_stat[i] = sum(vector[idx_0:idx_1])

    stat = 0
    for itm in interval_stat:
        if calc_stat(itm):
            stat += 1

    return stat, interval_stat

def calc_stat_non_zero(val):
    if val > 0:
        return True

    return False


def get_phone_stat(db, uid, time_0, time_1):
    cond = {
        'uid': uid,
        'event_time': {'$gte': time_0, '$lt': time_1},
        'event_source': 199
    }

    results = list(db['bio_events_199'].find(cond))

    gps_on = 0.0
    last_time = None
    ver = ''
    for item in results:
        fields = item['event_data'].split('; ')
        gps = 0
        try:
            gps = int(fields[0].replace('gps=', '').strip())
            gps_on += gps
        except:
            pass

        if last_time == None:
            last_time = item['event_time']

        if item['event_time'] >= last_time:
            ver = fields[1] + '; ' + fields[2]

    gps_ratio = 0
    if len(results) > 0:
        gps_ratio = gps_on / len(results)

    return gps_ratio, ver