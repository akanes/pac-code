'''
 * Created by ruiwang on 11/26/16
'''
import numpy
import copy

from ios.data_import import activity_map
from util import user_info

activity_map_android = {'still': 0, 'unknown': 1, 'tilting': 2, 'on foot': 3, 'on bike': 4, 'in vehicle': 5,
                        'walking': 6,
                        'running': 7, 'heartbeat': -1}
activity_map_ios = activity_map.activity_map

reverse_activity_map_android = {activity_map_android[item]: item for item in activity_map_android if
                                item != 'heartbeat'}
reverse_activity_map_ios = {activity_map_ios[item]: item for item in activity_map_ios}


def get_sec_idx(size, offset):
    return int(offset)


# get android activity samples. put sampels in a vector: 1Hz
def get_activity_raw_android(db, uid, start_time, end_time):
    event_list = db['activity_inferences'].find({'uid': uid, 'time': {'$gte': start_time, '$lte': end_time}})

    sample_val = -1 * numpy.ones(int((end_time - start_time).total_seconds()), dtype=int)

    for event in event_list:
        time = event['time']
        idx = get_sec_idx(len(sample_val), (time - start_time).total_seconds())
        try:
            val = int(event['type'])
            sample_val[idx] = val
        except:
            pass

    return sample_val


# get ios activity samples. put sampels in a vector: 1Hz
def get_activity_raw_ios(db, uid, start_time, end_time):
    event_list = db['ios_activity'].find({'uid': uid, 'time': {'$gte': start_time, '$lt': end_time}})

    sample_val = -1 * numpy.ones(int((end_time - start_time).total_seconds()), dtype=int)

    for event in event_list:
        time = event['time']
        idx = get_sec_idx(len(sample_val), (time - start_time).total_seconds())
        try:
            val = event['activity'][0]
        except:
            pass

        sample_val[idx] = val

    return sample_val


# extrapolate raw activity data - activity not sampled evenly
def extrapolate_raw_activity(activity_raw):
    activity_extra = copy.deepcopy(activity_raw)

    begin_idx = 0
    begin_act = 0
    if activity_extra[0] != -1:
        begin_act = activity_extra[0]

    for i in range(1, len(activity_raw)):
        if activity_extra[i] != -1 or i == len(activity_raw) - 1:
            if begin_act == 0 or activity_extra[i] != 0:
                activity_extra[begin_idx + 1:i] = begin_act
            else:
                if i - begin_idx > 15:
                    activity_extra[begin_idx + 1:i] = 0
                else:
                    activity_extra[begin_idx + 1:i] = begin_act

            begin_idx = i
            begin_act = activity_extra[i]

    activity_extra[-1] = activity_extra[-2]

    return activity_extra


# generate features
def generate_features(usr_type, activity_vector, epoch_def):
    if usr_type == user_info.USR_TYPE_ANDROID:
        act_map = reverse_activity_map_android
    elif usr_type == user_info.USR_TYPE_IOS:
        act_map = reverse_activity_map_ios
    else:
        raise Exception('invalid user type')

    epoch_def_secs = [
        [item[0] * 3600, item[1] * 3600]
        for item in epoch_def
        ]

    # generate a vector for each activity
    activity_vectors = {}
    for act in act_map:
        act_vector = numpy.zeros(len(activity_vector), dtype=int)
        act_vector[activity_vector == act] = 1

        activity_vectors[act_map[act]] = act_vector

    # all day feats
    activity_feat = {}

    # epoch features for each activity
    for act_name in activity_vectors:
        for idx in range(len(epoch_def_secs)):
            start_idx = epoch_def_secs[idx][0]
            end_idx = epoch_def_secs[idx][1]

            epoch_feat = sum(activity_vectors[act_name][start_idx:end_idx])
            activity_feat.update(
                {
                    'act_{}_ep_{}'.format(act_name, idx): epoch_feat
                }
            )

    return activity_feat


# compute the number of hours of the activity data
def compute_quality(activity_vector):
    count_thre = 1
    hours = 0;
    for i in range(24):
        start_idx = i * 3600
        end_idx = start_idx + 3600

        count = sum(activity_vector[start_idx:end_idx] != -1)
        if count > count_thre:
            hours += 1

    return hours
