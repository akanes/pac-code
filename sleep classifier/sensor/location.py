from scipy import spatial
from scipy.spatial.distance import _validate_vector
from sklearn.cluster import DBSCAN
from datetime import timedelta
from util.location_util import get_distance
import pymongo
import numpy
import json
from sensor import quality
import copy

DBSCAN_EPS = 30
DBSCAN_MIN_SAMPLE = 3


##################
# android
def get_location_trace_android(uid, db, start_time, end_time):
    # old version from funf
    query_cond = {'uid': uid, 'time': {
        '$gte': start_time, '$lt': end_time}}
    gps_list = list(db['raw_gps_location'].find(query_cond).sort('time', pymongo.ASCENDING))

    location_log = []
    for instance in gps_list:
        time = instance['time']
        try:
            lat = instance['latitude']
            lon = instance['longitude']

            location_log.append(
                {'time': time, 'lat': lat, 'lon': lon})
        except:
            pass

    # new version without funf
    if len(location_log) == 0:
        loc_collection = db['bio_events_2']
        gps_list = list(
            loc_collection.find({'uid': uid, 'event_time': {'$gte': start_time, '$lt': end_time}}).sort('event_time',
                                                                                                        pymongo.ASCENDING))
        for instance in gps_list:
            time = instance['event_time']
            data = instance['event_data']
            try:
                j = json.loads(data)
                lat = j['LATITUDE']
                lon = j['LONGITUDE']
                location_log.append(
                    {'time': time, 'lat': lat, 'lon': lon})
            except:
                pass

    return location_log


##################
# ios
def get_location_trace_ios(uid, db, start_time, end_time):
    loc_collection = db['ios_location']

    query_cond = {'uid': uid, '$or': [{'time': {'$gte': start_time, '$lt': end_time}}, {'$or': [
        {'start_time': {'$gte': start_time, '$lt': end_time}}, {'end_time': {'$gte': start_time, '$lt': end_time}}]}]}

    gps_list = list(loc_collection.find(query_cond).sort('time', pymongo.ASCENDING))

    location_log = []
    for instance in gps_list:
        time = instance['time']
        try:
            lat = instance['latitude']
            lon = instance['longitude']

            # old ios location format
            if 'start_time' in instance and 'end_time' in instance:
                loc_start_time = instance['start_time']
                loc_end_time = instance['end_time']
                if loc_start_time.year > 3000 or loc_end_time.year > 3000:
                    location_log.append(
                        {'time': time, 'lat': lat, 'lon': lon})
                    continue

                insert_time = loc_start_time
                while insert_time <= loc_end_time:
                    if insert_time >= start_time and insert_time < end_time:
                        location_log.append(
                            {'time': insert_time, 'lat': lat, 'lon': lon})

                    insert_time += timedelta(minutes=10)
            else:
                # latest ios location format
                location_log.append(
                    {'time': time, 'lat': lat, 'lon': lon})
        except:
            pass

    return location_log


##################
# location proc

def calc_location_distance(u, v):
    u = _validate_vector(u)
    v = _validate_vector(v)

    try:
        loc_dist = get_distance(u, v)
    except:
        loc_dist = spatial.distance.euclidean(u, v)

    return loc_dist


## cluster location trace into significant places
def find_sig_loc(loc_trace):
    if len(loc_trace) == 0:
        return [], [], []

    cord_list = []
    for item in loc_trace:
        cord_list.append([float(item['lat']), float(item['lon'])])

    cord_list = numpy.array(cord_list)
    db_res = DBSCAN(eps=DBSCAN_EPS, min_samples=DBSCAN_MIN_SAMPLE, metric=calc_location_distance).fit(cord_list)
    labels = db_res.labels_

    sig_loc = {}
    for i in range(max(labels) + 1):
        sig_loc[i] = numpy.mean(cord_list[labels == i], axis=0)

    # generate clustered coordinates
    clustered_trace = copy.deepcopy(loc_trace)
    for i in range(len(clustered_trace)):
        if labels[i] != -1:
            clustered_trace[i]['lat'] = sig_loc[labels[i]][0]
            clustered_trace[i]['lon'] = sig_loc[labels[i]][1]

    last_idx = -1
    trace = []
    for i in range(len(labels)):
        lbl = labels[i]
        if last_idx == -1 and lbl != -1:
            last_idx = i
        else:
            if labels[last_idx] != lbl:
                if labels[last_idx] != -1:
                    trace.append([last_idx, i - 1, labels[last_idx]])
                last_idx = i
    if last_idx != -1 and labels[last_idx] != -1:
        trace.append([last_idx, i, labels[last_idx]])

    trace_info_list = []
    for i in range(len(trace)):
        start_time = loc_trace[trace[i][0]]['time'] + timedelta(minutes=-5)
        end_time = loc_trace[trace[i][1]]['time'] + timedelta(minutes=5)
        duration = (end_time - start_time).total_seconds() / 3600.0
        loc_info = []
        gps = sig_loc[trace[i][2]]

        trace_info = {'start': start_time, 'end': end_time, 'duration': duration, 'loc_info': loc_info, 'gps': gps}
        trace_info_list.append(trace_info)

    return clustered_trace, labels, trace_info_list


## location features
def generate_location_features(location_trace, cluster_label, day, epoch_def):
    loc_feat = {}

    # epoch features
    for idx in range(len(epoch_def)):
        start_time = day + timedelta(hours=epoch_def[idx][0])
        end_time = day + timedelta(hours=epoch_def[idx][1])

        loc_num, loc_dist = get_loc_stat(location_trace, cluster_label, start_time, end_time)
        loc_feat.update(
            {
                'loc_visit_num_ep_{}'.format(idx): loc_num,
                'loc_dist_ep_{}'.format(idx): loc_dist,
            }
        )

    return loc_feat


def get_loc_stat(location_trace, cluster_label, start_time, end_time):
    loc_set = set()
    distance = 0
    last_coord = None
    for i in range(len(location_trace)):
        loc_visit = location_trace[i]
        if loc_visit['time'] < start_time:
            continue
        if loc_visit['time'] > end_time:
            break

        if cluster_label[i] != -1:
            loc_set.add((loc_visit['lat'], loc_visit['lon']))
        if last_coord is not None:
            distance += calc_location_distance(last_coord, [loc_visit['lat'], loc_visit['lon']])

        last_coord = [loc_visit['lat'], loc_visit['lon']]

    return len(loc_set), distance


## location quality
def get_location_quality(gps_trace, day):
    next_day = day + timedelta(days=1)
    loc_hours, _ = quality.get_data_quality_stat(gps_trace, day, next_day, lambda x: x['time'],
                                                 quality.calc_stat_non_zero)

    return loc_hours
