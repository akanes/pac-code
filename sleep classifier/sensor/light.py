'''
Created on Oct 28, 2014

@author: ruiwang
'''

from sensor import quality
from util.events_util import EventsUtil
import numpy
import warnings

# get light raw data
def get_light_amplitude(db, uid, start_time, end_time):
    event_util = EventsUtil()

    event_list = list(db['bio_events_141'].find(
        {'uid': uid, 'event_time': {'$gte': start_time, '$lt': end_time}}))

    light_readings = [numpy.NaN] * 24 * 3600;

    for item in event_list:
        idx = int((item['event_time'] - start_time).total_seconds())
        light_readings[idx] = float(event_util.to_Float(item['event_data']))

    return numpy.array(light_readings)


def generate_light_feat(light_readings, epoch_list):
    light_feat = {}
    # epochs
    for i in range(len(epoch_list)):
        epoch = epoch_list[i]
        start_idx = epoch[0] * 3600
        end_idx = epoch[1] * 3600
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            light_mean = numpy.nanmean(light_readings[start_idx:end_idx])
            light_std = numpy.nanvar(light_readings[start_idx:end_idx])

        light_feat['light_mean_ep_{}'.format(i)] = light_mean
        light_feat['light_std_ep_{}'.format(i)] = light_std

    return light_feat


def get_light_quality(light_readings):
    light_hours, _ = quality.eval_data_quality(numpy.isfinite(light_readings), 3600, quality.calc_stat_non_zero)

    return light_hours
