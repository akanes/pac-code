'''
 * Created by ruiwang on 11/26/16
'''

import json

# android
def get_ema_android(db, uid, start_time, end_time):
    resp = db['bio_events_900'].find({'event_source': 900, 'uid': uid, \
                                      'event_time': {'$gte': start_time, '$lt': end_time}})

    resp_list = []
    for instance in resp:
        try:
            resp_json = json.loads(instance['event_data'])
            resp_json['resp_time'] = instance['event_time']
            resp_list.append(resp_json)
        except:
            pass

    return resp_list

# ios
def get_ema_ios(db, uid, start_time, end_time):
    resp = db['ios_ema'].find({'uid': uid, 'time': {'$gte': start_time, '$lt': end_time}})

    resp_list = []
    for instance in resp:
        try:
            resp = instance['resp']
            resp_json = json.loads(resp)
            resp_json['resp_time'] = instance['time']
            resp_json['EMAName'] = instance['EMAName']
            resp_list.append(resp_json)
        except:
            pass

    return resp_list

# parse EMAs
def parse_emas(ema_list, parser_getter):
    ema_doc = {}
    for ema_itm in ema_list:
        try:
            parse_ema(ema_doc, ema_itm, parser_getter)
        except:
            pass

    return ema_doc

def parse_ema(ema_doc, resp_item, parser_getter):
    if ema_doc is None:
        ema_doc = {}

    ema_parser = parser_getter(resp_item)
    if ema_parser is None:
        return ema_doc

    return ema_parser(ema_doc, resp_item)