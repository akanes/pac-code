'''
Created on Apr 5, 2015

@author: ruiwang
'''
from datetime import datetime

import numpy

import SleepUnsupervisedPredictor
from ios.data_import import activity_map
from sensor import audio
from sensor import light
from sensor import phoneusage
from util import db_util
from util import user_info
from sensor import activity


def get_sleep_info(db, uid, day,os_type = user_info.USR_TYPE_ANDROID):

    start_time, end_time = SleepUnsupervisedPredictor.get_time_range(day);

    # get raw data
    if os_type == user_info.USR_TYPE_ANDROID:
        activity_vector = activity.get_activity_raw_android(db, uid, start_time, end_time)
        still_label = activity.activity_map_android['still']
        unlock_list, unlock_vector = phoneusage.get_lock_events_android(db, uid, start_time, end_time)
        audio_amp = audio.get_audio_amplitude_android(db, uid, start_time, end_time)
        light_readings = light.get_light_amplitude(db, uid, start_time, end_time)
    else:
        activity_vector = activity.get_activity_raw_ios(db, uid, start_time, end_time)
        still_label = activity_map.activity_map['stationary']
        unlock_list, unlock_vector = phoneusage.get_lock_events_ios(db, uid, start_time, end_time)
        audio_amp = audio.get_audio_amplitude_ios(db, uid, start_time, end_time)
        light_readings = numpy.zeros(int((end_time-start_time).total_seconds()))

    # pre-process raw data
    activity_still = numpy.logical_or(activity_vector == still_label, activity_vector == -1)
    screen_off = 1 - unlock_vector
    audio_amp[numpy.isnan(audio_amp)] = 0
    light_readings[numpy.isnan(light_readings)] = 0

    sleep_predictor = SleepUnsupervisedPredictor.SleepUnsupervisedPredictor()

    longest_start_idx, longest_end_idx, max_idx = sleep_predictor.predict(audio_amp, light_readings, activity_still, screen_off);

    sleep_duration = (longest_end_idx - longest_start_idx) / 8.0
    return sleep_duration, longest_start_idx, longest_end_idx, max_idx


if __name__ == '__main__':
    db = db_util.get_db('../conf/db_ns16f.conf')

    uid = 'u133@pbs16f'
    day = datetime(2016,12,6)
    os_type = 'ios'

    sleep_duration, longest_start_idx, longest_end_idx, max_idx = get_sleep_info(db, uid, day, os_type)

    print sleep_duration
