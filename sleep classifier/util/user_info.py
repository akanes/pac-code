'''
Created on Nov 16, 2015

@author: ruiwang
'''
from android.data_import import import_utils
from util import db_util
import pymongo
from datetime import datetime

db_collection_name = 'userinfo'
USR_FILENAME = '../conf/userlist.csv'
LOAD_FROM_FILE = True

USR_TYPE_ANDROID = 'android'
USR_TYPE_IOS = 'ios'

USER_INFO_COLLECTION = 'userinfo'
USER_INFO_INDEX = [('uid', pymongo.ASCENDING)]

# load user list from USR_FILENAME
def load_user_from_file(filename = USR_FILENAME):
    with open(filename) as fd:
        usr_list = fd.readlines()

    header_fields = [i.strip() for i in usr_list[0].strip().split(',')]
    date_fields = ['start_date', 'end_date']

    user_list = []
    for item in usr_list[1:]:
        fields = item.strip().split(',')
        doc = {}
        for i in range(len(header_fields)):
            doc[header_fields[i]] = fields[i].strip()

        for df in date_fields:
            date_field = None
            try:
                date_str = doc[df].split('/')
                if len(date_str) == 3:
                    date_field = datetime(int(date_str[2]) + 2000, int(date_str[0]), int(date_str[1]))
            except:
                pass

            doc[df] = date_field

        user_list.append(doc)

    return user_list

# import user list
def import_user_info(db):
    user_list = load_user_from_file()
    
    for doc in user_list:
        try:
            idx_doc = {'studyid':doc['studyid'], 'uid':doc['uid']}
            print doc
            db[db_collection_name].update(idx_doc, {'$set':doc}, upsert = True)
        except:
            try:
                db[db_collection_name].update(idx_doc, {'$set': doc}, upsert=True)
            except:
                print 'error', doc['EurekaID']
    
def get_user_info(db, cond = {}):
    if LOAD_FROM_FILE:
        return load_user_from_file()
    else:
        usr_info = db[db_collection_name].find(cond)
    
        return list(usr_info)

def update_home_loc(db, uid, lat, lon):
    import_utils.check_collection(db, USER_INFO_COLLECTION, USER_INFO_INDEX)

    doc = {'uid': uid, 'dorm': {'lat': lat, 'lon': lon}}

    try:
        db['userinfo'].update({'uid': uid}, {"$set": doc}, upsert=True)
    except:
        try:
            db['userinfo'].update({'uid': uid}, {"$set": doc}, upsert=True)
        except:
            pass

def get_home_loc(db, uid):
    doc = db['userinfo'].find_one({'uid': uid})
    home_loc = []
    if doc is not None and 'dorm' in doc:
        home_loc = [doc['dorm']['lat'], doc['dorm']['lon']]

    return home_loc

if __name__ == '__main__':
    db = db_util.get_db()
    
    #import_user_info(db)
    print get_user_info(db)
