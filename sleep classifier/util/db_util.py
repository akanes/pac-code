'''
Created on Jul 26, 2014

@author: RuiWang
'''
from datetime import datetime, timedelta

from bio_db_config import BioDbConfig
from pymongo.mongo_client import MongoClient

DEFAULT_DB_CONF = '../conf/db.conf'

#load db from the conf file
def get_db(conf_file=DEFAULT_DB_CONF):
    db, _= get_db_with_client(conf_file)

    return db

#load db and mongo client from the conf file
def get_db_with_client(conf_file=DEFAULT_DB_CONF):
    conf = BioDbConfig();
    conf.load_conf(conf_file)

    client = MongoClient(conf.db_uri)
    db = client[conf.database]

    return db, client

# convert timestamp from sqlite3 to datetime with millisecond part
def convert_sqlite3_time(timestamp):
    timestamp_sec = int(timestamp)
    timestamp_msec = int((timestamp - timestamp_sec) * 1000)

    convert_time = datetime.fromtimestamp(timestamp_sec)
    convert_time += timedelta(milliseconds=timestamp_msec)

    return convert_time
