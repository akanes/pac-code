'''
Created on Aug 1, 2013

@author: Rui Wang
'''
import ConfigParser

class BioDbConfig(object):
    '''
    load config
    '''

    def __init__(self):
        self.db_host=""
        self.db_port=0
        self.database=""
        
    def load_conf(self,filename):
        config = ConfigParser.RawConfigParser()
        config.read(filename)
        
        self.db_host = config.get('db_config', 'host')
        self.db_port = int(config.get('db_config', 'port'))
        self.database = config.get('db_config', 'database')
        
        self.db_user =  config.get('db_config', 'username')
        self.db_pwd = config.get('db_config', 'password')
        
        self.db_uri = "mongodb://{}:{}@{}:{}/{}".format(self.db_user,self.db_pwd,self.db_host,self.db_port,self.database)
