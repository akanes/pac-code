'''
Created on Aug 22, 2013

@author: Rui Wang
'''

from datetime import datetime
from math import radians, cos, sin, asin, sqrt
from geopy import distance
from operator import itemgetter

class BuildingCoor:
    def __init__(self, db):
        self.building_map = {}
        self.building_label = {}
        self.building_greek_type = {}
        self.dorm_coor = {}
        self.load_building(db)
        self.load_dorm(db)
        
    def load_building(self, db):
        for item in db['loc_dict'].find():
            self.building_map[item['loc']] = [item['lat'],item['lon']]
            self.building_label[item['loc']] = item['label']
            self.building_greek_type[item['loc']] = item['greek_type']
    
    def load_dorm(self, db):
        for item in db['userinfo'].find():
            usr = item['uid']
            dorm_coor = []
            if 'dorm' in item: 
                dorm_coor = item['dorm']
                self.dorm_coor[usr] = [dorm_coor['lat'], dorm_coor['lon']]
            
    def find_building(self, lat, lon):
        candidates = []
        for name in self.building_map:
            dist = gps_dist([lat, lon], self.building_map[name])
            if dist < 0.1:
                candidates.append([name, dist])
        
        candidates = sorted(candidates, key = itemgetter(1))
        return candidates
    
    def get_usr_dorm_coor(self, uid):
        if uid in self.dorm_coor:
            return self.dorm_coor[uid]
        
        return []
    
    def get_coordinates(self, loc):
        if loc in self.building_map:
            return self.building_map[loc]
        
        return None
    
    def get_labels(self, name):
        if name not in self.building_label:
            return []
        
        return self.building_label[name]
    
    def get_greek_type(self, name):
        if name not in self.building_greek_type:
            return ''
        
        return self.building_greek_type[name]

def gps_to_building(building_coor, gps_list):
    building_list = []
    for gps in gps_list:
        #[lat, lon, time]
        loc_candidate = building_coor.find_building(gps[0],gps[1])
        building_list.append([gps[2], loc_candidate])
        pass
    
    return building_list

def get_wifi_location_log(wifi_loc_record, enable_campus=False):
    
    location_list = []
    for entry in wifi_loc_record:
        loc_time = entry['time']
        loc = entry['location']
        if loc.startswith("in"):
            location = ((loc.split('['))[1].split(']'))[0]
            location_list.append([loc_time, 1, location])
        elif enable_campus:
            location_list.append([loc_time, 0, 'campus'])

    location_list = sorted(location_list, key=lambda elem:elem[0])

    wifi_loc = []
    time_thresh = 30 * 60
    last_loc = ""
    start_time = datetime(1970,1,1)
    end_time = datetime(1970,1,1)
    for entry in location_list:
        # new location
        if last_loc != entry[2] or (entry[0] - end_time).seconds > time_thresh:
            if (end_time - start_time).seconds > 30:
                wifi_loc.append([start_time, end_time, last_loc])

            last_loc = entry[2]
            start_time = entry[0]
            end_time = entry[0]
        else:
            end_time = entry[0]

    return wifi_loc

def find_loc(wifi_loc_record, loc_time):
    matchs = [loc for loc in wifi_loc_record if loc_time > loc[0] and loc_time < loc[1]]
    
    return matchs

def get_distance(loc0, loc1):
    return distance.distance(loc0,loc1).m

def gps_dist(coor1, coor2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    lat1 = coor1[0];
    lon1 = coor1[1];
    
    lat2 = coor2[0];
    lon2 = coor2[1];
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    # c = 2 * atan2(sqrt(a),sqrt(1-a) ) 

    # 6367 km is the radius of the Earth
    km = 6367 * c
    return km 