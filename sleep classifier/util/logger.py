'''
 * Created by ruiwang on 11/14/16
'''
import logging.handlers
import sys

'''
set logging parameters
'''
# logger's name
LOGGER_NAME = "StudentLifeLog"
# logging level
LOGGING_LEVEL = logging.DEBUG

## syslog configs
# "on Linux it's usually '/dev/log' but on OS/X it's '/var/run/syslog'."
# see: https://docs.python.org/2/library/logging.handlers.html#logging.handlers.SysLogHandler
SYS_LOGGER_ADDR = '/dev/log'
if sys.platform.startswith('darwin'):
    SYS_LOGGER_ADDR = '/var/run/syslog'

'''
init logger
'''
app_logger = logging.getLogger(LOGGER_NAME)
app_logger.setLevel(LOGGING_LEVEL)

#create syslog handler
handler = logging.handlers.SysLogHandler(address = SYS_LOGGER_ADDR)
# create the formatter
formatter = logging.Formatter('%(name)s: [%(levelname)s] %(message)s')
# plug this formatter into your handler(s)
handler.setFormatter(formatter)
# add handler
app_logger.addHandler(handler)

'''
logger functions
'''
# debug
def d(msg, exc_info = False):
    app_logger.debug(msg, exc_info = exc_info)

# INFO
def i(msg, exc_info = False):
    app_logger.info(msg, exc_info = exc_info)

# error
def e(msg, exc_info = False):
    app_logger.error(msg, exc_info = exc_info)
